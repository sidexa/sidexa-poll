## Modal service

```
function open(){
  var scope = $rootScope.$new(true), defer = $q.defer(), fn = {};
  scope.fn = fn;
  scope.form = {
    title: ''
  };

  fn.cancel = function(){
    scope.modal.hide().then(function(){
      defer.reject();
    });
  };
  fn.create = function(){
    defer.resolve(scope.form);
    scope.modal.hide();
  };

  $ionicModal.fromTemplateUrl(templateUrl, {scope: scope}).then(function(modal){
    scope.modal = modal;
    scope.modal.show();
  });

  scope.$on('modal.hidden', function(){
    scope.$destroy();
  });
  scope.$on('$destroy', function(){
    scope.modal.remove();
  });

  return defer.promise;
}
```

## List option

```
<ion-list can-swipe="true">
  <ion-item>
    <ion-option-button></ion-option-button>
  </ion-item>
</ion-list>
```

## Pull-to-refresh

```
<ion-refresher pulling-text="" on-refresh="fn.refresh()"></ion-refresher>
$scope.$broadcast('scroll.refreshComplete');
```

## Search

```
<div class="bar bar-subheader item-input-inset">
  <label class="item-input-wrapper">
    <i class="icon ion-search placeholder-icon"></i>
    <input type="search" placeholder="Rechercher ..." ng-model="search">
  </label>
  <button class="button button-clear" ng-click="search=''">Annuler</button>
</div>
<ion-content class="has-subheader">
</ion-content>
```

## Firebase search

https://sidexa-poll.firebaseio.com/users.json?orderBy="name"&equalTo="loic"
