# Sidexa poll

## Getting started

## TODO

- créer un sondage
- afficher les sondages existants
- supprimer un sondage
- actualiser les sondages
- chercher dans les sondages
- aller sur un sondage
- créer une proposition
- lister les propositions
- chercher dans les propositions
- actualiser le sondage
- supprimer/voter pour une proposition
- afficher le score d'une proposition
- trier les propositions par score

- confirmation pour une suppression
- aller sur une proposition
- actualiser la proposition
- voter pour une proposition
- offline (services Storage & Http)
- créer un utilisateur pour poster un sondage, proposition, commentaire ou voter
    - modal de création
    - enregistrement en local et sur firebase
    - récupération de l'utilisateur pour créer les sondages...
- afficher quelles propositions ont été votées et empêcher de les voter 2 fois
- setting screen to logout or change user

## Data

```
Poll {
  title: 'poll title',
  created: 1456063556339,
  createdBy: '123',
  createdByName: 'Loïc',
  proposals: [{
    title: 'proposal title',
    description: 'multiline proposal description',
    created: 1456063556339,
    createdBy: '123',
    createdByName: 'Loïc',
    votes: [{
      userId: '123',
      created: 1456063556339,
      value: 1
    }],
    comments: [{
      userId: '123',
      username: 'Loïc',
      created: 1456063556339,
      text: 'comment text'
    }]
  }]
}
```

Backend : https://www.firebase.com/docs/rest/api/
