angular.module('app', ['ionic'])
.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'app/layout/layout.html',
    controller: 'LayoutCtrl'
  })
  .state('app.pollList', {
    url: '/polls',
    views: {
      'menuContent': {
        templateUrl: 'app/polls/pollList.html',
        controller: 'PollListCtrl'
      }
    }
  })
  .state('app.poll', {
    url: '/polls/:pollId',
    views: {
      'menuContent': {
        templateUrl: 'app/polls/poll.html',
        controller: 'PollCtrl'
      }
    }
  })
  .state('app.proposal', {
    url: '/polls/:pollId/proposals/:proposalId',
    views: {
      'menuContent': {
        templateUrl: 'app/polls/proposal.html',
        controller: 'ProposalCtrl'
      }
    }
  })
  .state('app.user', {
    url: '/user',
    views: {
      'menuContent': {
        templateUrl: 'app/user/user.html',
        controller: 'UserCtrl'
      }
    }
  });
  $urlRouterProvider.otherwise('/app/polls');
});
