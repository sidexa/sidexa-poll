(function(){
  'use strict';
  angular.module('app')
    .factory('Backend', Backend)
    .factory('Http', Http);

  function Backend($q, Http, Storage, CreateUserModal){
    var baseUrl = 'https://sidexa-poll.firebaseio.com';
    var polls = 'polls';
    return {
      getPolls: getPolls,
      getPoll: getPoll,
      getProposal: getProposal,
      createPoll: createPoll,
      deletePoll: deletePoll,
      createProposal: createProposal,
      deleteProposal: deleteProposal,
      voteProposal: voteProposal,
      commentProposal: commentProposal,
      getUser: getUser,
      getUserByName: getUserByName,
      createUser: createUser,
      getOrCreateUser: getOrCreateUser
    };

    function getPolls(){
      return Http.get(baseUrl+'/'+polls+'.json').then(function(res){
        return parsePolls(res.data);
      });
    }
    function getPoll(pollId){
      return Storage.getUser().then(function(userOpt){
        return Http.get(baseUrl+'/'+polls+'/'+pollId+'.json').then(function(res){
          res.data.id = pollId;
          return parsePoll(res.data, userOpt);
        });
      });
    }
    function getProposal(pollId, proposalId){
      // TODO
    }
    function createPoll(title){
      return getOrCreateUserWithModal().then(function(user){
        var poll = {
          title: title,
          created: Date.now(),
          createdBy: user.id,
          createdByName: user.name,
          proposals: []
        };
        return Http.post(baseUrl+'/'+polls+'.json', poll).then(function(res){
          var id = res.data.name;
          return id;
        });
      });
    }
    function deletePoll(pollId){
      return Http.delete(baseUrl+'/'+polls+'/'+pollId+'.json').then(function(res){});
    }
    function createProposal(pollId, title, description){
      return getOrCreateUserWithModal().then(function(user){
        var proposal = {
          title: title,
          description: description,
          created: Date.now(),
          createdBy: user.id,
          createdByName: user.name,
          votes: [],
          comments: []
        };
        return Http.post(baseUrl+'/'+polls+'/'+pollId+'/proposals.json', proposal).then(function(res){
          var id = res.data.name;
          return id;
        });
      });
    }
    function deleteProposal(pollId, proposalId){
      return Http.delete(baseUrl+'/'+polls+'/'+pollId+'/proposals/'+proposalId+'.json').then(function(res){});
    }
    function voteProposal(pollId, proposalId){
      return getOrCreateUserWithModal().then(function(user){
        var vote = {
          created: Date.now(),
          createdBy: user.id,
          createdByName: user.name,
          value: 1
        };
        return Http.post(baseUrl+'/'+polls+'/'+pollId+'/proposals/'+proposalId+'/votes.json', vote).then(function(res){
          var id = res.data.name;
          return id;
        });
      });
    }
    function commentProposal(pollId, proposalId, text){
      // TODO
    }
    function getUser(userId){
      return Http.get(baseUrl+'/users/'+userId+'.json').then(function(res){
        res.data.id = userId;
        return parseUser(res.data);
      });
    }
    function getUserByName(name){
      return Http.get(baseUrl+'/users.json?orderBy="name"&equalTo="'+name+'"').then(function(res){
        var userArr = parseUsers(res.data);
        return userArr.length > 0 ? userArr[0] : $q.reject({message: 'User not found'});
      });
    }
    function createUser(name){
      var user = {
        name: name,
        created: Date.now()
      };
      return Http.post(baseUrl+'/users.json', user).then(function(res){
        var id = res.data.name;
        return id;
      });
    }


    function getOrCreateUser(name){
      return getUserByName(name).then(null, function(err){
        return createUser(name).then(function(userId){
          return getUser(userId);
        });
      });
    }
    function getOrCreateUserWithModal(){
      return Storage.getUser().then(function(user){
        if(user){
          return user;
        } else {
          return CreateUserModal.open().then(function(data){
            return getOrCreateUser(data.name);
          }).then(function(user){
            Storage.setUser(user);
            return user;
          });
        }
      });
    }

    function parsePolls(polls, userOpt){
      return obj2arr(polls).map(function(poll){
        return parsePoll(poll, userOpt);
      });
    }
    function parsePoll(poll, userOpt){
      poll.proposals = obj2arr(poll.proposals).map(function(proposal){
        return parseProposal(proposal, userOpt);
      });
      return poll;
    }
    function parseProposal(proposal, userOpt){
      proposal.votes = obj2arr(proposal.votes);
      proposal.score = _.sum(_.map(proposal.votes, 'value'));
      proposal.isVotedByMe = userOpt ? !!_.find(proposal.votes, {createdBy: userOpt.id}) : false;
      return proposal;
    }
    function parseUsers(users){
      return obj2arr(users).map(parseUser);
    }
    function parseUser(user){
      return user;
    }
    function obj2arr(obj){
      var arr = [];
      for(var i in obj){
        obj[i].id = i;
        arr.push(obj[i]);
      }
      return arr;
    }
  }
  function Http($http, $q, Storage){
    return {
      get: get,
      post: post,
      delete: _delete
    };

    function get(url){
      return $http.get(url).then(function(res){
        Storage.set(url, res);
        return res;
      }, function(err){
        return Storage.get(url).then(function(data){
          return data || $q.reject(err);
        });
      });
    }
    function post(url, data){
      return $http.post(url, data);
    }
    function _delete(url){
      return $http.delete(url);
    }
  }
})();
