(function(){
  'use strict';
  angular.module('app')
    .factory('CreateProposalModal', CreateProposalModal);

  function CreateProposalModal($rootScope, $q, $ionicModal){
    var templateUrl = 'app/common/partials/create-proposal-modal.html';
    return {
      open: open
    };
    
    function open(){
      var defer = $q.defer();
      var scope = $rootScope.$new(true);
      var fn = {};
      var form = {
        title: '',
        description: ''
      };
      scope.fn = fn;
      scope.form = form;

      fn.cancel = function(){
        scope.modal.hide().then(function(){
          defer.reject();
        });
      };
      fn.save = function(){
        defer.resolve(form);
        scope.modal.hide();
      };

      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: scope
      }).then(function(modal){
        scope.modal = modal;
        scope.modal.show();
      });

      scope.$on('modal.hidden', function(){
        scope.$destroy();
      });
      scope.$on('$destroy', function(){
        scope.modal.remove();
      });

      return defer.promise;
    }
  }
})();
