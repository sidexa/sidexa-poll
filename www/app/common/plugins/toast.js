(function(){
  'use strict';
  angular.module('app')
    .factory('ToastPlugin', ToastPlugin);

  function ToastPlugin($q, $ionicPlatform){
    return {
      show: show
    };

    function show(text){
      return $ionicPlatform.ready().then(function(){
        var defer = $q.defer();
        window.plugins.toast.show(text,'long', 'bottom', function(res){
          defer.resolve(res);
        }, function(err){
          defer.reject(err);
        });
        return defer.promise;
      });
    }
  }





  /**************************
   *                        *
   *      Browser Mock      *
   *                        *
   **************************/
  ionic.Platform.ready(function(){
    if(!(ionic.Platform.isAndroid() || ionic.Platform.isIOS() || ionic.Platform.isIPad())){
      if(!window.plugins){window.plugins = {};}
      if(!window.plugins.toast){
        window.plugins.toast = {
          show: function(message, duration, position, successCallback, errorCallback){
            // durations : short, long
            // positions : top, center, bottom
            // default: short bottom
            console.log('Toast: '+message);
            if(successCallback){window.setTimeout(successCallback('OK'), 0);}
          },
          showShortTop: function(message, successCallback, errorCallback){this.show(message, 'short', 'top', successCallback, errorCallback);},
          showShortCenter: function(message, successCallback, errorCallback){this.show(message, 'short', 'center', successCallback, errorCallback);},
          showShortBottom: function(message, successCallback, errorCallback){this.show(message, 'short', 'bottom', successCallback, errorCallback);},
          showLongTop: function(message, successCallback, errorCallback){this.show(message, 'long', 'top', successCallback, errorCallback);},
          showLongCenter: function(message, successCallback, errorCallback){this.show(message, 'long', 'center', successCallback, errorCallback);},
          showLongBottom: function(message, successCallback, errorCallback){this.show(message, 'long', 'bottom', successCallback, errorCallback);}
        };
      }
    }
  });
})();
