(function(){
  'use strict';
  angular.module('app')
    .factory('Storage', Storage);

  function Storage($q){
    var keys = {
      user: 'user'
    };
    return {
      getUser: getUser,
      setUser: setUser,
      removeUser: removeUser,
      get: get,
      set: set,
      remove: remove
    };

    function getUser(){
      return get(keys.user);
    }
    function setUser(user){
      return set(keys.user, user);
    }
    function removeUser(){
      return remove(keys.user);
    }
    function get(key){
      try {
        return $q.when(JSON.parse(localStorage.getItem(key)));
      } catch(e){
        return $q.when();
      }
    }
    function set(key, value){
      return $q.when(localStorage.setItem(key, JSON.stringify(value)));
    }
    function remove(key){
      return $q.when(localStorage.removeItem(key));
    }
  }
})();
