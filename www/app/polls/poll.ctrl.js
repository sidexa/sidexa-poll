(function(){
  'use strict';
  angular.module('app')
    .controller('PollCtrl', PollCtrl);

  function PollCtrl($scope, $stateParams, $ionicListDelegate, $ionicPopup, Backend, CreateProposalModal){
    var data = {}, fn = {};
    $scope.data = data;
    $scope.fn = fn;

    $scope.$on('$ionicView.enter', function(){
      refresh();
    });

    fn.createProposal = function(){
      CreateProposalModal.open().then(function(data){
        Backend.createProposal($stateParams.pollId, data.title, data.description).then(function(proposalId){
          refresh();
        });
      });
    };
    fn.delete = function(proposal){
      $ionicPopup.confirm({
        title: 'Supprimer la proposition ?',
        template: proposal.title
      }).then(function(res){
        $ionicListDelegate.closeOptionButtons();
        if(res){
          Backend.deleteProposal($stateParams.pollId, proposal.id).then(function(){
            refresh();
          });
        }
      });
    };
    fn.vote = function(proposal){
      $ionicListDelegate.closeOptionButtons();
      Backend.voteProposal($stateParams.pollId, proposal.id).then(function(){
        refresh();
      });
    };
    fn.refresh = function(){
      refresh().then(function(){
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    function refresh(){
      return Backend.getPoll($stateParams.pollId).then(function(poll){
        console.log('poll', poll);
        data.poll = poll;
      });
    }
  }
})();
