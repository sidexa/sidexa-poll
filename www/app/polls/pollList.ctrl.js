(function(){
  'use strict';
  angular.module('app')
    .controller('PollListCtrl', PollListCtrl);

  function PollListCtrl($scope, $state, $ionicListDelegate, $ionicPopup, CreatePollModal, Backend, ToastPlugin){
    var data = {}, fn = {};
    $scope.data = data;
    $scope.fn = fn;

    $scope.$on('$ionicView.enter', function(){
      refresh();
    });

    fn.createPoll = function(){
      CreatePollModal.open().then(function(data){
        Backend.createPoll(data.title).then(function(pollId){
          $state.go('app.poll', {pollId: pollId});
        });
      });
    };
    fn.delete = function(poll){
      $ionicPopup.confirm({
        title: 'Supprimer le sondage ?',
        template: poll.title
      }).then(function(res){
        $ionicListDelegate.closeOptionButtons();
        if(res){
          Backend.deletePoll(poll.id).then(function(){
            ToastPlugin.show('Sondage supprimé');
            refresh();
          });
        }
      });
    };
    fn.refresh = function(){
      refresh().then(function(){
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    function refresh(){
      return Backend.getPolls().then(function(polls){
        console.log('polls', polls);
        data.polls = polls;
      });
    }
  }
})();
