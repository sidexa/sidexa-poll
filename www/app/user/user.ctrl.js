(function(){
  'use strict';
  angular.module('app')
    .controller('UserCtrl', UserCtrl);

  function UserCtrl($scope, Backend, Storage, CreateUserModal){
    var data = {}, fn = {};
    $scope.data = data;
    $scope.fn = fn;

    $scope.$on('$ionicView.enter', function(){
      refresh();
    });

    fn.changeUser = function(){
      return CreateUserModal.open().then(function(data){
        return Backend.getOrCreateUser(data.name);
      }).then(function(user){
        Storage.setUser(user).then(function(){
          refresh();
        });
      });
    };
    fn.logout = function(){
      Storage.removeUser().then(function(){
        refresh();
      });
    };

    function refresh(){
      Storage.getUser().then(function(user){
        data.user = user;
      });
    }
  }
})();
